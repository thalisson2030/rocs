def dfs(start):
    stack = [start]
    start.seen = True

    while stack:
        top = stack.pop()
        top.color = '#c00'

        for neighbor in top.neighbors():
            if not neighbor.seen:
                stack.append(neighbor)
                neighbor.seen = True


dfs(Document.nodes()[0])
