#include "edgewrapperpy.h"

#include "edge.h"
#include "documentwrapperpy.h"

#include <KLocalizedString>

using namespace GraphTheory;

EdgeWrapperPy::EdgeWrapperPy(EdgePtr edge, const DocumentWrapperPy &documentWrapperPy) :
    m_edge(edge),
    m_documentWrapper(documentWrapperPy)
{
}

EdgeWrapperPy::~EdgeWrapperPy()
{
}

EdgePtr EdgeWrapperPy::edge() const
{
    return m_edge;
}

int EdgeWrapperPy::type() const
{
    return m_edge->type()->id();
}

void EdgeWrapperPy::setType(int typeId)
{
    EdgeTypePtr newType = m_edge->type();
    if (newType->id() == typeId) {
        return;
    }
    const auto edgeTypes = m_edge->from()->document()->edgeTypes();
    for (const EdgeTypePtr &type : edgeTypes) {
        if (type->id() == typeId) {
            newType = type;
            break;
        }
    }
    if (newType == m_edge->type()) {
        QString command = QString("edge.type = (%1)").arg(typeId);
        m_documentWrapper.processMessage(i18nc("@info:shell", "%1: edge type ID %2 not registered", command, typeId), Messenger::ErrorMessage);
        return;
    }
    m_edge->setType(newType);
}

NodeWrapperPy EdgeWrapperPy::to() const
{
    return NodeWrapperPy(m_edge->to(), m_documentWrapper);
}

NodeWrapperPy EdgeWrapperPy::from() const
{
    return NodeWrapperPy(m_edge->from(), m_documentWrapper);
}

bool EdgeWrapperPy::directed() const
{
    return m_edge->type()->direction() == EdgeType::Unidirectional;
}

pybind11::object EdgeWrapperPy::dynamicProperty(const std::string &name) const
{
    auto prop = m_edge->dynamicProperty(QString::fromStdString(name));
    if (prop.isValid()) {
        return prop.value<pybind11::object>();
    }
    return pybind11::none();
}

void EdgeWrapperPy::setDynamicProperty(const std::string &name, const pybind11::object &value)
{
    m_edge->setDynamicProperty(QString::fromStdString(name), QVariant::fromValue(value));
}
