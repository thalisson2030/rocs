#ifndef EDGEWRAPPERPY_H
#define EDGEWRAPPERPY_H

#include "nodewrapperpy.h"

namespace GraphTheory
{
class DocumentWrapperPy;

class EdgeWrapperPy
{
public:
    EdgeWrapperPy(EdgePtr edge, const DocumentWrapperPy &documentWrapperPy);
    ~EdgeWrapperPy();

    EdgePtr edge() const;

    int type() const;
    void setType(int typeId);

    NodeWrapperPy to() const;
    NodeWrapperPy from() const;
    bool directed() const;

    pybind11::object dynamicProperty(const std::string &name) const;
    void setDynamicProperty(const std::string &name, const pybind11::object &value);
private:
    EdgePtr m_edge;
    const DocumentWrapperPy &m_documentWrapper;
};
}

#endif
