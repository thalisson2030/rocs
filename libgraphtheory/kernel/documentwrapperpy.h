#ifndef DOCUMENTWRAPPERPY_H
#define DOCUMENTWRAPPERPY_H

#include "nodewrapperpy.h"
#include "edgewrapperpy.h"
#include "messenger.h"

namespace GraphTheory
{
class DocumentWrapperPy
{
public:
    DocumentWrapperPy(GraphDocumentPtr document, Messenger &messenger);
    ~DocumentWrapperPy();

    uint getObjects() const;
    std::string documentName() const;

    std::vector<EdgeWrapperPy> edges() const;
    std::vector<EdgeWrapperPy> edgesByType(int type) const;
    std::vector<NodeWrapperPy> nodes() const;
    std::vector<NodeWrapperPy> nodesByType(int type) const;
    NodeWrapperPy createNode(int x, int y);
    EdgeWrapperPy createEdge(NodeWrapperPy from, NodeWrapperPy to);
    void remove(NodeWrapperPy node);
    void remove(EdgeWrapperPy edge);

    void processMessage(const QString &messageString, Messenger::MessageType type) const;

private:
    GraphDocumentPtr m_document;
    Messenger &m_messenger;
};
}

#endif
