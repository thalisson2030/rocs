#include "documentwrapperpy.h"

#include "edge.h"

#include <KLocalizedString>

using namespace GraphTheory;

DocumentWrapperPy::DocumentWrapperPy(GraphDocumentPtr document, Messenger &messenger) :
    m_document(document),
    m_messenger(messenger)
{
}

DocumentWrapperPy::~DocumentWrapperPy()
{
}

uint DocumentWrapperPy::getObjects() const
{
    return m_document->objects();
}

std::string DocumentWrapperPy::documentName() const
{
    return m_document->documentName().toStdString();
}

std::vector<EdgeWrapperPy> DocumentWrapperPy::edges() const
{
    std::vector<EdgeWrapperPy> result;
    for (auto edge : m_document->edges()) {
        result.emplace_back(edge, *this);
    }
    return result;
}

std::vector<EdgeWrapperPy> DocumentWrapperPy::edgesByType(int type) const
{
    EdgeTypePtr edgeType;
    for (const auto &t : m_document->edgeTypes()) {
        if (t->id() == type) {
            edgeType = t;
            break;
        }
    }
    if (not edgeType) {
        QString command = QString("Document.edges(%1)").arg(type);
        Q_EMIT m_messenger.message(i18nc("@info:shell", "%1: edge type ID %2 not registered", command, type), Messenger::ErrorMessage);
        return {};
    }

    std::vector<EdgeWrapperPy> result;
    for (const auto &edge : m_document->edges(edgeType))
        result.emplace_back(edge, *this);
    return result;
}

std::vector<NodeWrapperPy> DocumentWrapperPy::nodes() const
{
    std::vector<NodeWrapperPy> result;
    for (auto node : m_document->nodes()) {
        result.emplace_back(node, *this);
    }
    return result;
}

std::vector<NodeWrapperPy> DocumentWrapperPy::nodesByType(int type) const
{
    NodeTypePtr typePtr;
    for (const auto &t : m_document->nodeTypes()) {
        if (t->id() == type) {
            typePtr = t;
            break;
        }
    }
    if (!typePtr) {
        QString command = QString("Document.nodes(%1)").arg(type);
        Q_EMIT m_messenger.message(i18nc("@info:shell", "%1: node type ID %2 not registered", command, type), Messenger::ErrorMessage);
        return {};
    }

    std::vector<NodeWrapperPy> result;
    for (const auto &node : m_document->nodes(typePtr))
        result.emplace_back(node, *this);
    return result;
}

NodeWrapperPy DocumentWrapperPy::createNode(int x, int y)
{
    auto node = Node::create(m_document);
    node->setX(x);
    node->setY(y);
    return NodeWrapperPy(node, *this);
}

EdgeWrapperPy DocumentWrapperPy::createEdge(NodeWrapperPy from, NodeWrapperPy to)
{
    auto edge = Edge::create(from.node(), to.node());
    return EdgeWrapperPy(edge, *this);
}

void DocumentWrapperPy::remove(NodeWrapperPy node)
{
    node.node()->destroy();
}

void DocumentWrapperPy::remove(EdgeWrapperPy edge)
{
    edge.edge()->destroy();
}

void DocumentWrapperPy::processMessage(const QString &message, Messenger::MessageType type) const
{
    Q_EMIT m_messenger.message(message, type);
}
