#ifndef KERNELPY_H
#define KERNELPY_H

#include "graphtheory_export.h"
#include "typenames.h"
#include "node.h"
#include "graphdocument.h"
#include "messenger.h"
#include "kernel_interface.h"
#include "documentwrapperpy.h"

namespace GraphTheory
{
/**
 * \class Kernel
 */
class GRAPHTHEORY_EXPORT KernelPy : public KernelInterface
{
    Q_OBJECT

public:
    KernelPy();

    ~KernelPy() override;

    /**
     * execute python @p script on @p document
     */
    void execute(GraphTheory::GraphDocumentPtr document, const QString &script) override;
    void stop() override;

    void attachDebugger() override;
    void detachDebugger() override;
    void triggerInterruptAction() override;

private Q_SLOTS:
    /** process all incoming messages and resend them afterwards**/
    void processMessage(const QString &message, Messenger::MessageType type) override;

Q_SIGNALS:
    void message(const QString &message, Messenger::MessageType type);
    void executionFinished();
};
}

#endif
