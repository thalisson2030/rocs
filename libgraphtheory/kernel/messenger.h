#ifndef MESSENGER_H
#define MESSENGER_H

#include <QObject>

namespace GraphTheory
{
class Messenger : public QObject
{
    Q_OBJECT

public:
    enum MessageType { InfoMessage, WarningMessage, ErrorMessage };

public:
    Messenger() = default;
    ~Messenger() = default;

Q_SIGNALS:
    void message(const QString &messageString, MessageType type) const;

private:
    Q_DISABLE_COPY(Messenger)
};
}

#endif
