#include "nodewrapperpy.h"

#include "edge.h"
#include "edgewrapperpy.h"
#include "documentwrapperpy.h"

#include <KLocalizedString>

using namespace GraphTheory;

NodeWrapperPy::NodeWrapperPy(NodePtr node, const DocumentWrapperPy &documentWrapperPy) :
    m_node(node),
    m_documentWrapper(documentWrapperPy)
{
}

NodeWrapperPy::~NodeWrapperPy()
{
}

NodePtr NodeWrapperPy::node() const
{
    return m_node;
}

int NodeWrapperPy::id() const
{
    return m_node->id();
}

double NodeWrapperPy::x() const
{
    return m_node->x();
}

void NodeWrapperPy::setX(double x)
{
    if (x == NodeWrapperPy::x())
        return;
    m_node->setX(x);
}

double NodeWrapperPy::y() const
{
    return m_node->y();
}

void NodeWrapperPy::setY(double y)
{
    if (y == NodeWrapperPy::y())
        return;
    m_node->setY(y);
}

std::string NodeWrapperPy::color() const
{
    return m_node->color().name().toStdString();
}

void NodeWrapperPy::setColor(const std::string &colorString)
{
    QColor color = QColor(colorString.c_str());
    if (color == m_node->color()) {
        return;
    }
    m_node->setColor(color);
}

int NodeWrapperPy::type() const
{
    return m_node->type()->id();
}

void NodeWrapperPy::setType(int typeId)
{
    NodeTypePtr newType = m_node->type();
    if (newType->id() == typeId) {
        return;
    }
    const auto nodeTypes = m_node->document()->nodeTypes();
    for (const auto &type : nodeTypes) {
        if (type->id() == typeId) {
            newType = type;
            break;
        }
    }
    if (newType == m_node->type()) {
        QString command = QString("node.type = (%1)").arg(typeId);
        Q_EMIT m_documentWrapper.processMessage(i18nc("@info:shell", "%1: node type ID %2 not registered", command, typeId), Messenger::ErrorMessage);
        return;
    }
    m_node->setType(newType);
}

std::vector<EdgeWrapperPy> NodeWrapperPy::edges() const
{
    std::vector<EdgeWrapperPy> edges;
    for (const auto &edge : m_node->edges())
        edges.emplace_back(edge, m_documentWrapper);
    return edges;
}

std::vector<EdgeWrapperPy> NodeWrapperPy::edgesByType(int type) const
{
    auto typePtr = findEdgeType(type);
    if (!typePtr) {
        QString command = QString("node.edges(%1)").arg(type);
        Q_EMIT m_documentWrapper.processMessage(i18nc("@info:shell", "%1: edge type ID %2 not registered", command, type), Messenger::ErrorMessage);
        return {};
    }
    std::vector<EdgeWrapperPy> edges;
    const auto nodeEges = m_node->edges(typePtr);
    for (const auto &edge : nodeEges)
        edges.emplace_back(edge, m_documentWrapper);
    return edges;
}

std::vector<EdgeWrapperPy> NodeWrapperPy::inEdges() const
{
    std::vector<EdgeWrapperPy> edges;
    for (const auto &edge : m_node->inEdges())
        edges.emplace_back(edge, m_documentWrapper);
    return edges;
}

std::vector<EdgeWrapperPy> NodeWrapperPy::inEdgesByType(int type) const
{
    auto typePtr = findEdgeType(type);
    if (!typePtr) {
        QString command = QString("node.inEdges(%1)").arg(type);
        Q_EMIT m_documentWrapper.processMessage(i18nc("@info:shell", "%1: edge type ID %2 not registered", command, type), Messenger::ErrorMessage);
        return {};
    }
    std::vector<EdgeWrapperPy> edges;
    const auto nodeEges = m_node->inEdges(typePtr);
    for (const auto &edge : nodeEges)
        edges.emplace_back(edge, m_documentWrapper);
    return edges;
}

std::vector<EdgeWrapperPy> NodeWrapperPy::outEdges() const
{
    std::vector<EdgeWrapperPy> edges;
    for (const auto &edge : m_node->outEdges())
        edges.emplace_back(edge, m_documentWrapper);
    return edges;
}

std::vector<EdgeWrapperPy> NodeWrapperPy::outEdgesByType(int type) const
{
    auto typePtr = findEdgeType(type);
    if (!typePtr) {
        QString command = QString("node.outEdges(%1)").arg(type);
        Q_EMIT m_documentWrapper.processMessage(i18nc("@info:shell", "%1: edge type ID %2 not registered", command, type), Messenger::ErrorMessage);
        return {};
    }
    std::vector<EdgeWrapperPy> edges;
    const auto nodeEges = m_node->outEdges(typePtr);
    for (const auto &edge : nodeEges)
        edges.emplace_back(edge, m_documentWrapper);
    return edges;
}

std::vector<NodeWrapperPy> NodeWrapperPy::neighbors() const
{
    std::vector<NodeWrapperPy> neighbors;
    for (auto edge : m_node->edges()) {
        if (m_node == edge->to()) {
            neighbors.emplace_back(edge->from(), m_documentWrapper);
        } else {
            neighbors.emplace_back(edge->to(), m_documentWrapper);
        }
    }

    return neighbors;
}

std::vector<NodeWrapperPy> NodeWrapperPy::neighborsByType(int type) const
{
    auto typePtr = findEdgeType(type);
    if (!typePtr) {
        QString command = QString("node.neighbors(%1)").arg(type);
        Q_EMIT m_documentWrapper.processMessage(i18nc("@info:shell", "%1: edge type ID %2 not registered", command, type), Messenger::ErrorMessage);
        return {};
    }
    std::vector<NodeWrapperPy> neighbors;
    for (auto edge : m_node->edges(typePtr)) {
        if (m_node == edge->to()) {
            neighbors.emplace_back(edge->from(), m_documentWrapper);
        } else {
            neighbors.emplace_back(edge->to(), m_documentWrapper);
        }
    }

    return neighbors;
}

std::vector<NodeWrapperPy> NodeWrapperPy::predecessors() const
{
    std::vector<NodeWrapperPy> predecessors;
    for (auto edge : m_node->inEdges()) {
        if (edge->type()->direction() == EdgeType::Unidirectional) {
            predecessors.emplace_back(edge->from(), m_documentWrapper);
            continue;
        } else {
            if (m_node == edge->from()) {
                predecessors.emplace_back(edge->to(), m_documentWrapper);
            } else {
                predecessors.emplace_back(edge->from(), m_documentWrapper);
            }
        }
    }

    return predecessors;
}

std::vector<NodeWrapperPy> NodeWrapperPy::predecessorsByType(int type) const
{
    auto typePtr = findEdgeType(type);
    if (!typePtr) {
        QString command = QString("node.predecessors(%1)").arg(type);
        Q_EMIT m_documentWrapper.processMessage(i18nc("@info:shell", "%1: edge type ID %2 not registered", command, type), Messenger::ErrorMessage);
        return {};
    }
    std::vector<NodeWrapperPy> predecessors;
    for (auto edge : m_node->inEdges(typePtr)) {
        if (edge->type()->direction() == EdgeType::Unidirectional) {
            predecessors.emplace_back(edge->from(), m_documentWrapper);
            continue;
        } else {
            if (m_node == edge->from()) {
                predecessors.emplace_back(edge->to(), m_documentWrapper);
            } else {
                predecessors.emplace_back(edge->from(), m_documentWrapper);
            }
        }
    }

    return predecessors;
}

std::vector<NodeWrapperPy> NodeWrapperPy::successors() const
{
    std::vector<NodeWrapperPy> successors;
    for (auto edge : m_node->outEdges()) {
        if (edge->type()->direction() == EdgeType::Unidirectional) {
            successors.emplace_back(edge->from(), m_documentWrapper);
            continue;
        } else {
            if (m_node == edge->from()) {
                successors.emplace_back(edge->to(), m_documentWrapper);
            } else {
                successors.emplace_back(edge->from(), m_documentWrapper);
            }
        }
    }

    return successors;
}

std::vector<NodeWrapperPy> NodeWrapperPy::successorsByType(int type) const
{
    auto typePtr = findEdgeType(type);
    if (!typePtr) {
        QString command = QString("node.successors(%1)").arg(type);
        Q_EMIT m_documentWrapper.processMessage(i18nc("@info:shell", "%1: edge type ID %2 not registered", command, type), Messenger::ErrorMessage);
        return {};
    }
    std::vector<NodeWrapperPy> successors;
    for (auto edge : m_node->outEdges(typePtr)) {
        if (edge->type()->direction() == EdgeType::Unidirectional) {
            successors.emplace_back(edge->from(), m_documentWrapper);
            continue;
        } else {
            if (m_node == edge->from()) {
                successors.emplace_back(edge->to(), m_documentWrapper);
            } else {
                successors.emplace_back(edge->from(), m_documentWrapper);
            }
        }
    }

    return successors;
}

EdgeTypePtr NodeWrapperPy::findEdgeType(int typeId) const
{
    for (const auto &type : m_node->document()->edgeTypes()) {
        if (type->id() == typeId) {
            return type;
        }
    }
    return {};
}

pybind11::object NodeWrapperPy::dynamicProperty(const std::string &name) const
{
    auto prop = m_node->dynamicProperty(QString::fromStdString(name));
    if (prop.isValid()) {
        return prop.value<pybind11::object>();
    }
    return pybind11::none();
}

void NodeWrapperPy::setDynamicProperty(const std::string &name, const pybind11::object &value)
{
    m_node->setDynamicProperty(QString::fromStdString(name), QVariant::fromValue(value));
}
