#ifndef KERNEL_INTERFACE_H
#define KERNEL_INTERFACE_H

#include "graphdocument.h"
#include "messenger.h"

#include <QObject>

namespace GraphTheory
{
class KernelInterface : public QObject
{
    Q_OBJECT

public:
    virtual void execute(GraphDocumentPtr document, const QString &script) = 0;
    virtual void stop() = 0;

    virtual void attachDebugger() = 0;
    virtual void detachDebugger() = 0;
    virtual void triggerInterruptAction() = 0;

public Q_SLOTS:
    virtual void processMessage(const QString &message, Messenger::MessageType type) = 0;

Q_SIGNALS:
    void message(const QString &message, GraphTheory::Messenger::MessageType type);
    void executionFinished();
};
}

#endif