#ifndef NODEWRAPPERPY_H
#define NODEWRAPPERPY_H

#include "node.h"

#include <pybind11/stl.h>

namespace GraphTheory
{
class EdgeWrapperPy;
class DocumentWrapperPy;

class NodeWrapperPy
{
public:
    NodeWrapperPy(NodePtr node, const DocumentWrapperPy &documentWrapperPy);
    ~NodeWrapperPy();

    NodePtr node() const;

    int id() const;

    double x() const;
    void setX(double x);
    double y() const;
    void setY(double y);

    std::string color() const;
    void setColor(const std::string &colorString);
    int type() const;
    void setType(int typeId);

    std::vector<EdgeWrapperPy> edges() const;
    std::vector<EdgeWrapperPy> edgesByType(int type) const;
    std::vector<EdgeWrapperPy> inEdges() const;
    std::vector<EdgeWrapperPy> inEdgesByType(int type) const;
    std::vector<EdgeWrapperPy> outEdges() const;
    std::vector<EdgeWrapperPy> outEdgesByType(int type) const;
    std::vector<NodeWrapperPy> neighbors() const;
    std::vector<NodeWrapperPy> neighborsByType(int type) const;
    std::vector<NodeWrapperPy> predecessors() const;
    std::vector<NodeWrapperPy> predecessorsByType(int type) const;
    std::vector<NodeWrapperPy> successors() const;
    std::vector<NodeWrapperPy> successorsByType(int type) const;

    pybind11::object dynamicProperty(const std::string &name) const;
    void setDynamicProperty(const std::string &name, const pybind11::object &value);
private:
    EdgeTypePtr findEdgeType(int typeId) const;

private:
    NodePtr m_node;
    const DocumentWrapperPy &m_documentWrapper;
};
}

Q_DECLARE_METATYPE(pybind11::object)

#endif
