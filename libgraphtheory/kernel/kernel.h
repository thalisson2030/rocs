/*
 *  SPDX-FileCopyrightText: 2014 Andreas Cord-Landwehr <cordlandwehr@kde.org>
 *
 *  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#ifndef KERNEL_H
#define KERNEL_H

#include "graphdocument.h"
#include "graphtheory_export.h"
#include "node.h"
#include "typenames.h"
#include "messenger.h"
#include "kernel_interface.h"

#include <QObject>
#include <QScriptEngine>

#include <QAction>

namespace GraphTheory
{
class KernelPrivate;

/**
 * \class Kernel
 */
class GRAPHTHEORY_EXPORT Kernel : public KernelInterface
{
    Q_OBJECT

public:
    Kernel();

    ~Kernel() override;

    /**
     * execute javascript @p script on @p document and @return result as reported by engine
     */
    void execute(GraphTheory::GraphDocumentPtr document, const QString &script) override;
    void stop() override;

    void attachDebugger() override;
    void detachDebugger() override;
    void triggerInterruptAction() override;

private Q_SLOTS:
    /** process all incoming messages and resend them afterwards**/
    void processMessage(const QString &message, GraphTheory::Messenger::MessageType type) override;

Q_SIGNALS:
    void message(const QString &message, GraphTheory::Messenger::MessageType type);
    void executionFinished();

private:
    const QScopedPointer<KernelPrivate> d;
};
}

#endif
