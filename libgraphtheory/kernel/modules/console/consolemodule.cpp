/*
 *  SPDX-FileCopyrightText: 2013-2014 Andreas Cord-Landwehr <cordlandwehr@kde.org>
 *
 *  SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
 */

#include "consolemodule.h"

using namespace GraphTheory;

ConsoleModule::ConsoleModule(QObject *parent)
    : QObject(parent)
{
}

ConsoleModule::~ConsoleModule()
{
}

QList<QPair<GraphTheory::Messenger::MessageType, QString>> ConsoleModule::backlog() const
{
    return m_backlog;
}

void ConsoleModule::clear()
{
    m_backlog.clear();
}

void ConsoleModule::log(const QString &messageString)
{
    m_backlog.append(qMakePair<Messenger::MessageType, QString>(Messenger::InfoMessage, messageString));
    Q_EMIT message(messageString, Messenger::InfoMessage);
}

void ConsoleModule::debug(const QString &messageString)
{
    m_backlog.append(qMakePair<Messenger::MessageType, QString>(Messenger::WarningMessage, messageString));
    Q_EMIT message(messageString, Messenger::WarningMessage);
}

void ConsoleModule::error(const QString &messageString)
{
    m_backlog.append(qMakePair<Messenger::MessageType, QString>(Messenger::ErrorMessage, messageString));
    Q_EMIT message(messageString, Messenger::ErrorMessage);
}
