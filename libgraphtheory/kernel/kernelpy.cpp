#include "kernelpy.h"

#include "documentwrapperpy.h"
#include "edgewrapperpy.h"

#include <KLocalizedString>
#include <pybind11/embed.h>
#include <pybind11/pybind11.h>
#include <pybind11/iostream.h>

#include <sstream>

using namespace GraphTheory;


PYBIND11_EMBEDDED_MODULE(wrappers, m)
{
    pybind11::class_<DocumentWrapperPy>(m, "GraphDocumentWrapper")
        .def("objects", &DocumentWrapperPy::getObjects)
        .def("documentName", &DocumentWrapperPy::documentName)
        .def("edges", &DocumentWrapperPy::edges)
        .def("edges", &DocumentWrapperPy::edgesByType)
        .def("nodes", &DocumentWrapperPy::nodes)
        .def("nodes", &DocumentWrapperPy::nodesByType)
        .def("createNode", &DocumentWrapperPy::createNode)
        .def("createEdge", &DocumentWrapperPy::createEdge)
        .def("remove", static_cast<void (DocumentWrapperPy::*) (NodeWrapperPy)>(&DocumentWrapperPy::remove))
        .def("remove", static_cast<void (DocumentWrapperPy::*) (EdgeWrapperPy)>(&DocumentWrapperPy::remove));

    pybind11::class_<EdgeWrapperPy>(m, "EdgeWrapper")
        .def("__setattr__", [](EdgeWrapperPy &self, const std::string &name, pybind11::object value) {
            if (name == "type")
                self.setType(value.cast<int>());
            else
                self.setDynamicProperty(name, value);
        })
        .def("__getattr__", &EdgeWrapperPy::dynamicProperty)
        .def("to", &EdgeWrapperPy::to)
        .def("_from", &EdgeWrapperPy::from)
        .def("directed", &EdgeWrapperPy::directed)
        .def_property("type", &EdgeWrapperPy::type, &EdgeWrapperPy::setType);

    pybind11::class_<NodeWrapperPy>(m, "NodeWrapper")
        .def("__setattr__", [](NodeWrapperPy &self, const std::string &name, pybind11::object value) {
            if (name == "type")
                self.setType(value.cast<int>());
            else if (name == "color")
                self.setColor(value.cast<std::string>());
            else if (name == "x")
                self.setX(value.cast<double>());
            else if (name == "y")
                self.setY(value.cast<double>());
            else
                self.setDynamicProperty(name, value);
        })
        .def("__getattr__", &NodeWrapperPy::dynamicProperty)
        .def("id", &NodeWrapperPy::id)
        .def_property("color", &NodeWrapperPy::color, &NodeWrapperPy::setColor)
        .def_property("type", &NodeWrapperPy::type, &NodeWrapperPy::setType)
        .def_property("x", &NodeWrapperPy::x, &NodeWrapperPy::setX)
        .def_property("y", &NodeWrapperPy::y, &NodeWrapperPy::setY)
        .def("edges", &NodeWrapperPy::edges)
        .def("edges", &NodeWrapperPy::edgesByType)
        .def("inEdges", &NodeWrapperPy::inEdges)
        .def("inEdges", &NodeWrapperPy::inEdgesByType)
        .def("outEdges", &NodeWrapperPy::outEdges)
        .def("outEdges", &NodeWrapperPy::outEdgesByType)
        .def("neighbors", &NodeWrapperPy::neighbors)
        .def("neighbors", &NodeWrapperPy::neighborsByType)
        .def("predecessors", &NodeWrapperPy::predecessors)
        .def("predecessors", &NodeWrapperPy::predecessorsByType)
        .def("successors", &NodeWrapperPy::successors)
        .def("successors", &NodeWrapperPy::successorsByType);
}

class PyOutputBuffer
{
public:
    PyOutputBuffer(Messenger &messenger, Messenger::MessageType type)
        : m_messenger(messenger)
        , m_type(type)
    {
    }

    void write(const std::string &s)
    {
        m_buffer << s;
        flushCompleteLines();
    }

    void flushCompleteLines()
    {
        std::string line;
        int last_tellg = -1;
        while (std::getline(m_buffer, line, '\n'))
        {
            last_tellg = m_buffer.tellg();
            if (last_tellg > 0)
                Q_EMIT m_messenger.message(QString::fromStdString(line), m_type);
        }

        m_buffer.str(std::string());
        m_buffer.clear();

        if (last_tellg < 0)
            m_buffer << line;
    }

    ~PyOutputBuffer()
    {
        for (std::string line; std::getline(m_buffer, line); )
            Q_EMIT m_messenger.message(QString::fromStdString(line), m_type);
    }

private:
    Messenger &m_messenger;
    Messenger::MessageType m_type;
    std::stringstream m_buffer;
};

/// BEGIN: Kernel
KernelPy::KernelPy()
{
}

KernelPy::~KernelPy()
{
}

void KernelPy::execute(GraphDocumentPtr document, const QString &script)
{
    using namespace pybind11::literals;

    pybind11::scoped_interpreter guard{};
    pybind11::module_::import("wrappers");

    Messenger messenger;
    DocumentWrapperPy docWrapper = DocumentWrapperPy(document, messenger);
    auto pyBind_locals = pybind11::dict("doc"_a = docWrapper);

    connect(&messenger, &Messenger::message, this, &KernelPy::processMessage);

    try
    {
        PyOutputBuffer stdout(messenger, Messenger::InfoMessage);
        PyOutputBuffer stderr(messenger, Messenger::ErrorMessage);

        auto sysm = pybind11::module::import("sys");
        sysm.attr("stdout").attr("write") = pybind11::cpp_function([&stdout](const std::string &s) { stdout.write(s); });
        sysm.attr("stderr").attr("write") = pybind11::cpp_function([&stderr](const std::string &s) { stderr.write(s); });

        pybind11::exec("Document = locals()['doc']\n" + script.toStdString(), pybind11::globals(), pyBind_locals);
    }
    catch (const std::exception &e)
    {
        processMessage(QString::fromStdString(e.what()).replace("\n\n", "\n"), Messenger::InfoMessage);
    }
    processMessage(i18nc("@info status message after successful script execution", "<i>Execution Finished</i>"), Messenger::InfoMessage);

    disconnect(&messenger, &Messenger::message, this, &KernelPy::processMessage);

    Q_EMIT executionFinished();
}

void KernelPy::stop()
{
    // TO-DO
}

void KernelPy::processMessage(const QString &messageString, Messenger::MessageType type)
{
    Q_EMIT message(messageString, type);
}

void KernelPy::attachDebugger()
{
    // TO-DO
}

void KernelPy::detachDebugger()
{
    // TO-DO
}

void KernelPy::triggerInterruptAction()
{
    // TO-DO
}

// END: Kernel
